<?php

    class StudentInfo{

        public $std_id="";     // member variable or class property

        public $std_name="";
        public $std_cgpa="";

        public function set_id($std_id){

            $this->std_id=$std_id;      // here the first std_id calls global member variable but the next one is the local variable of the function set_id
            var_dump($this->std_id);
            echo "<br>";
            echo $this->std_id;
            echo "<br>";
        }

        public function set_id2(){
                $std_id="world";
                echo $this-> $std_id;
    }
        public function set_std_id($std_id){

            $this->std_id=$std_id;
    }

        public function set_std_name($std_name){

            $this->std_name=$std_name;
        }
        public function set_std_cgpa($std_cgpa){

            $this->std_cgpa=$std_cgpa;
        }
        public function get_std_id(){
            return $this->std_id;
        }

        public function get_std_name(){
            return $this->std_name;
        }

        public function get_std_cgpa(){
            return $this->std_cgpa;
        }

    }
    $obj= new StudentInfo;
   // $obj->set_id("SEIP146537");

    $obj->set_std_id("STUDENT");
    $obj->set_std_name("Farzana");
    $obj->set_std_cgpa("3.96");

    echo $obj->get_std_id()."<br>";
    echo $obj->get_std_name()."<br>";
    echo $obj->get_std_cgpa()."<br>";


